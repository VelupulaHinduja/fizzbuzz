package com.tw;

public class FizzBuzz {

    private static final Integer HUNDRED = 100;
    private static final Integer ONE = 1;
    private static final Integer THREE = 3;
    private static final Integer FIVE = 5;

    public String convert(Integer number) {
        if (number % THREE == 0 && number % FIVE == 0)
            return "FizzBuzz";
        else if (number % THREE == 0)
            return "Fizz";
        else if (number % FIVE == 0)
            return "Buzz";
        return String.valueOf(number);
    }

    public void print() {
        for (Integer number = ONE; number <= HUNDRED; number++) {
            System.out.println(convert(number));
        }

    }
}
